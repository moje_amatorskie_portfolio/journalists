<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Journalist;

class JournalistsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('journalists')->insert([
            ['name'=>'Agnieszka Cegielska',
            'description'=>'Agnieszka Cegielska to jedna z najbardziej lubianych prezenterek pogody w TVN. Ma już 7-letniego synka Franciszka. Teraz marzy o drugim dziecku. – Rodzina jest najważniejsza. Nie tylko chciałabym, ale wręcz pragnęłabym znów zostać mamą – zwierza się Faktowi Agnieszka Cegielska. Jak widać pogodynka bardzo chciałaby, żeby jej syn miał rodzeństwo.',
            'image'=>'images/agnieszka_cegielska.jpeg'],
            ['name'=>'Paulina Sykut',
            'description1'=>'W dzieciństwie występowała w chórze kościelnym, a później w Zespole Pieśni i Tańca „Powiśle”. W czasach licealnych amatorsko śpiewała piosenki poetyckie w Puławskim Ośrodku Kultury. Jako nastolatka została laureatką wielu ogólnopolskich konkursów i festiwali poezji śpiewanej. Zdobyła m.in. pierwszą nagrodę w XIV Ogólnopolskim Konkursie Literatury Współczesnej w Ostrowcu Świętokrzyskim',
            'image'=>'images/paulina_sykut.jpeg'],
            ['name'=>'Anita Werner',
            'description'=>'Karierę w mediach rozpoczęła od Wizji Sport. Następnie pojawiła się w TVN24, z którym związana jest od początku istnienia stacji (od 2001). Na antenie TVN24 prowadzi Fakty po południu oraz Fakty po Faktach. W przeszłości prowadziła również programy: Damę Pik, Serwisy informacyjne, Poranek TVN24, Prosto z Polski, Magazyn 24 godziny, Raport wieczorny, Studio 24, Studio Europa i Wybory 2005.',
            'image'=>'images/anita_werner.jpeg'],
            ['name'=>'Beata Tadla',
            'description'=>'3 września 1992 zagrała gościnnie postać Damy Dworu w sztuce w reżyserii Krzysztofa Gradowskiego Miss baśni i bajek, która została wystawiona w Centrum Sztuki - Teatrze Dramatycznym w Legnicy[15]. Następnie, 31 marca 2012 wystąpiła na deskach Teatru Syrena w premierowym spektaklu Trójka do potęgi (reż. Wojciech Malajkat)[1]. W tym samym roku zaśpiewała gościnnie w piosence Karp 2012[16]. Wzięła udział serialach:  Ocalony świat (2014) oraz była lektorką w filmie dokumentalnym Moje Camino',
            'image'=>'images/beata_tadla.jpeg'],
            ['name'=>'Agnieszka Konarska',
            'description'=>'Wokalistka, absolwentka Wydziału Wokalnego – Sekcja Piosenki w Zespole Państwowych Szkół Muzycznych im. Fryderyka Chopina. Swoją przygodę z muzyką rozpoczęła w wieku pięciu lat. Dziś jest laureatką wielu ogólnopolskich i międzynarodowych festiwali. Zdobyła Grand Prix Międzynarodowego Festiwalu Piosenki Rosyjskiej w Zielonej Górze, Grand Prix Międzynarodowego Festiwalu Twórców Piosenki Polskiej oraz Grand Prix Europejskiego Festiwalu Piosenki „Discovery” w Warnie. Występowała w telewizyjnej „Szansie na sukces”, a także koncercie organizowanym przez TVP Polonia „Z miłością z Białorusi”. Współpracuje z wieloma znakomitymi muzykami i zespołami. Obecnie przygotowuje się do wydania swojej pierwszej autorskiej płyty',
            'image'=>'images/agnieszka_konarska.jpeg'],
            ['name'=>'Anna Kałczyńska',
            'description'=>'Pracowała w redakcji Pegaza w TVP1 i w TVP Polonia. W 2002 roku dołączyła do zespołu TVN24. Była współautorką i wydawcą magazynu Studio Europa, a od lutego 2006 roku również jego prezenterką. Od grudnia 2003 do września 2014 roku prowadziła serwisy informacyjne TVN24. Była też reporterką stacji. W latach 2007–2010 razem z Łukaszem Grassem prowadziła magazyn Polska i świat. Od lutego do grudnia 2014 razem z Jackiem Pałasińskim prowadziła cotygodniowy magazyn Tu Europa',
            'image'=>'images/anna_kalczynska.jpeg'],
            ['name'=>'Anna Mucha',
            'description'=>'W 2006, wraz z Maciejem Orłosiem, była konferansjerką na 31. Festiwalu Polskich Filmów Fabularnych w Gdyni. Jej kreacja oraz niewybredne komentarze wywołały wzburzenie środowiska; przed pokazem jednego z filmów wypowiedziała zdanie: Już nie będę przynudzać, bo na tym filmie i tak pośniecie, a przed pokazem Summer Love, będącym debiutem 38-letniego Piotra Uklańskiego, stwierdziła: Zaraz zobaczymy, czy lepiej debiutować późno, czy wcale[2][3][4]. Ostatecznie organizator podjął decyzję o odsunięciu aktorki z roli gospodyni wieczoru, tłumacząc fakt słowami: formuła prowadzenia, zaprezentowana przez Panią Annę Muchę',
            'image'=>'images/anna_mucha.jpeg'],
            ['name'=>'Barbara Kurdej',
            'description'=>'W 2014 zagrała główną rolę w komedii romantycznej Dzień dobry, kocham cię!, do którego nagrała również utwór o tym samym tytule, w duecie z raperem Liberem. Od 2014 wciela się w rolę Joanny Chodakowskiej (wcześniej Tarnowskiej) w serialu TVP2 M jak miłość. Zagrała także w filmach: Wkręceni 2, 7 rzeczy, których nie wiecie o facetach, Narzeczony na niby i Pech to nie grzech. Od 10 września 2017 gra Marię Biernacką w serialu W rytmie sercaW 2014 zagrała główną rolę w komedii romantycznej Dzień dobry, kocham cię!, do którego nagrała również utwór o tym samym tytule, w duecie z raperem Liberem. Od 2014 wciela się w rolę Joanny Chodakowskiej (wcześniej Tarnowskiej) w serialu TVP2 M jak miłość. Zagrała także w filmach: Wkręceni 2, 7 rzeczy, których nie wiecie o facetach, Narzeczony na niby i Pech to nie grzech. Od 10 września 2017 gra Marię Biernacką w serialu W rytmie serca',
            'image'=>'images/barbara_kurdej.jpeg'],
            ['name'=>'Joanna Liszowska',
            'description'=>'W 2001 zadebiutowała jako aktorka na scenie Teatru Wielkiego w Warszawie w roli Anny I w Siedmiu grzechach głównych[1], a za rolę Onej w przedstawieniu Tadeusza Różewicza Rajski ogródek otrzymała wyróżnienie podczas XIX Festiwalu Szkół Teatralnych w Łodzi. Występowała na deskach Teatru Starego i STU w Krakowie oraz Operetki Krakowskiej, a także warszawskich teatrów: Komedia, Studio Buffo i Ateneum. Grała w musicalach: Chicago[1] (główna rola), Panna Tutli Putli[1] i Romeo i Julia[1], a także w spektaklu muzycznym Człowiek z La Manchy[1] oraz multimedialnym widowisku muzycznym Vichry',
            'image'=>'images/joanna_liszowska.jpeg'],
            ['name'=>'Małgorzata Socha',
            'description'=>'Na wielkim ekranie debiutowała w 1999 epizodyczną rolą w filmie Moja Angelika w reżyserii Stanisława Kuźnika. W 2000 zagrała jedną z głównych ról w filmie Waldemara Szarka To my. W kolejnych latach grała jedną z głównych ról w filmach: Śniadanie do łóżka Krzysztofa Langa (2010), Weekend Cezarego Pazury (2011) oraz dwóch filmach Na wielkim ekranie debiutowała w 1999 epizodyczną rolą w filmie Moja Angelika w reżyserii Stanisława Kuźnika. W 2000 zagrała jedną z głównych ról w filmie Waldemara Szarka To my. W kolejnych latach grała jedną z głównych ról w filmach: Śniadanie do łóżka Krzysztofa Langa (2010), Weekend Cezarego Pazury (2011) oraz dwóch filmach',
            'image'=>'images/malgorzata_socha.jpeg'],
            ['name'=>'Małgorzata Tomaszewska',
            'description'=>'W latach 2014-2016 prezentowała pogodę w Telewizji Polsat. W 2015 roku wystąpiła w programie Dancing with the Stars. Taniec z gwiazdami[4]. W parze z Robertem Kochankiem zajęli 9. miejsce. W 2016 roku rozpoczęła pracę w Telewizji WP. Poprowadziła wrocławski koncert sylwestrowy 2016/2017 transmitowany w Telewizji WP[5], oraz w latach 2017-2018 była gospodynią programu W latach 2014-2016 prezentowała pogodę w Telewizji Polsat. W 2015 roku wystąpiła w programie Dancing with the Stars. Taniec z gwiazdami[4]. W parze z Robertem Kochankiem zajęli 9. miejsce.',
            'image'=>'images/malgorzata_tomaszewska.jpeg'],
            ['name'=>'Marzena Kawa',
            'description'=>'Program ma charakter poradnikowy. W magazynie poruszane są sprawy życia codziennego. W każdym odcinku omawianych jest kilka tematów, o których dyskutują zaproszeni goście oraz specjaliści z różnych dziedzin. W audycji prezentowane są również występy muzyków, a za stały punkt programu uznaje się również kącik kulinarny. Program ma charakter poradnikowy. W magazynie poruszane są sprawy życia codziennego. W każdym odcinku omawianych jest kilka tematów, o których dyskutują zaproszeni goście oraz specjaliści z różnych dziedzin. W audycji prezentowane są również występy muzyków, a za stały punkt programu uznaje się również kącik kulinarny. Program ma charakter poradnikowy. W magazynie poruszane są sprawy życia codziennego. W każdym odcinku omawianych jest kilka tematów',
            'image'=>'images/marzena_kawa.jpeg'],
        ]);
    }
}
