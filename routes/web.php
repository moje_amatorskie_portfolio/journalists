<?php

// use App;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/lang/{locale}','LocalizationController@index');
Route::get('/verify/success',function(){
    return view('journalists.index')->with('notice',__('Verification successful'));
});
Route::get('/journalists','JournalistController@index');//->middleware('verified');
Auth::routes(['verify' => true]);
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/admin',function(){
    return view('layouts.admin');
});
Route::post('/logout',function(){
    auth()->logout();
    return view('auth.logout');
});
Route::get('/logout',function(){
    return view('journalists.index');
})->name('logout');
Route::get('/reset_successful',function(){
   return view('journalists.index')->with('notice','Twoje hasło zostało zmienione');
});
Route::get('/search','JournalistController@search');
Route::get('/add','JournalistController@create');//->middleware('verified');
Route::post('/add','JournalistController@store');
Route::get('/edit/{journalist}','JournalistController@edit');
Route::get('/delete/{journalist}','JournalistController@destroy');
Route::post('/update/{journalist}','JournalistController@update');
Route::post('/post_votes','VoteController@store');
Route::get('/setting','SettingController@create');
Route::post('/setting','SettingController@store');
Route::get('/vote/{journalist}','VoteController@index');
Route::get('/vote/delete/{journalist}/{vote}','VoteController@destroy');
Route::get('/diagram_json','JournalistController@diagramJson');
Route::get('/diagram','JournalistController@showDiagram');
Route::get('/admin/manage','JournalistController@manage');
Route::get('/email/votes','EmailController@ship');//->middleware('verified');
Route::get('/email/verify/link',function(){
    //return view('journalists.index')->with('notice',__('Verification successful'));
})->middleware('verified');
