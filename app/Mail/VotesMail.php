<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Journalist;
use PDF;


class VotesMail extends Mailable
{
    use Queueable, SerializesModels;

     /**
     * Create a new message instance.
     *
     * @return void
     */

    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $journalistVotesMailList = Journalist::all();
        $pdf = PDF::loadView('emails.email_pdf',compact('journalistVotesMailList'));
        return $this->view('emails.email_content')->attachData($pdf->download(),'Lista.pdf',['mime' => 'application/pdf']);
    }
}
