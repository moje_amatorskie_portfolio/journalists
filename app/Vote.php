<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
    protected $fillable = [
        'ip', 'id_journalist',
    ];

    public function journalist()
    {
        return $this->belongsTo('App\Journalist');
    }
}
