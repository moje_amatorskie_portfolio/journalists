<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Vote;

class Journalist extends Model
{
    public function getShortDescriptionAttribute($full_description)
    {
        return substr($this->description,0,50)."...<h4><a href='".url("/edit/$this->id")."'>"."czytaj dalej</a></h4>";
    }

    public function getImageAttribute($image)
    {
         return '../storage/app/'.$image;
    }

    public function setNameAttribute($value)
    {
        $this->attributes['name']=ucwords($value);
    }

    public function votes()
    {
        return $this->hasMany('App\Vote','id_journalist');
    }
}
