<?php

namespace App\Http\Controllers;

// use App\Http\Controllers\Controller;
use App\Journalist;
use Illuminate\Http\Request;
use App\Mail\VotesMail;
use Illuminate\Support\Facades\Mail;

class EmailController extends Controller
{
    /**
     * Ship the given order.
     *
     * @param  Request  $request
     * @param  int  $orderId
     * @return Response
     */
    public function ship(Request $request)
    {
        Mail::to($request->user())->send(new VotesMail());
        return view('journalists.index')->with('notice',__('messages.votes_list_has_been_sent'));

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Journalist  $journalist
     * @return \Illuminate\Http\Response
     */
    public function show(Journalist $journalist)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Journalist  $journalist
     * @return \Illuminate\Http\Response
     */
    public function edit(Journalist $journalist)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Journalist  $journalist
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Journalist $journalist)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Journalist  $journalist
     * @return \Illuminate\Http\Response
     */
    public function destroy(Journalist $journalist)
    {
        //
    }
}
