<?php

namespace App\Http\Controllers;

use App\Vote;
use App\Journalist;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;  //for ArrLexcept
use Illuminate\Support\Facades\Auth;

class VoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Journalist $journalist)
    {
        $votes=$journalist->votes()->get();
        $journalist_id=$journalist->id;

        return view('journalists.vote',compact('votes','journalist_id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Vote::where('ip',$request->ip())->delete();
        $all_votes_sent_with_token=$request->all();
        $all_votes_sent=Arr::except($all_votes_sent_with_token,['_token']);
        foreach ($all_votes_sent as $id_journalist=>$is_checked) {
            $vote=new Vote();
            $vote->ip=$request->ip();
            $vote->id_journalist=$id_journalist;
            $vote->save();
        }
        return view('journalists.index')->with('notice',__('messages.your_votes_have_been_saved'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Vote  $vote
     * @return \Illuminate\Http\Response
     */
    public function show(Vote $vote)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Vote  $vote
     * @return \Illuminate\Http\Response
     */
    public function edit(Vote $vote)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Vote  $vote
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vote $vote)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Vote  $vote
     * @return \Illuminate\Http\Response
     */
    public function destroy(Journalist $journalist,Vote $vote)
    {
        if (Auth::check() && Auth::user()->is_admin) {
        $vote->delete();   //tutaj dałem takie zabezpieczenie przed usunięciem,
        //ponieważ teoretycznie ktoś niezalogowanuy może zgadnąć link i po
        //wpisaniu np. /vote/22/33 by się skasowało bez uprawnień
        }
        $journalist_id=$journalist->id;
        return redirect("/vote/$journalist_id");
    }
}
