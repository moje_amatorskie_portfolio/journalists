<?php

namespace App\Http\Controllers;

use App\Journalist;
use App\Setting;
use App\Http\Requests\JournalistRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class JournalistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $journalists = Journalist::all();
        $setting = Setting::all()->first();
        $votes_limit_per_ip = $setting->votes_limit_per_ip;
        //print_r(auth()->user());
        return view('journalists.index',compact('journalists','votes_limit_per_ip'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('journalists.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(JournalistRequest $request)
    {
        $journalist=new Journalist();
        $image_path='images/placeholder.jpeg';
        if ($request->journalist_image) {
            $image_path=$request->file('journalist_image')->store('images');
        }
        $journalist->name=$request->journalist_name;
        $journalist->description=$request->journalist_description;
        $journalist->image=$image_path;
        $journalist->save();
        return view('layouts.admin')->with('journalist_added',true);
    }

    public function manage()
    {
        $journalists=Journalist::paginate(10);
        $highest_vote_count=0;
        foreach ($journalists as $journalist) {
            $temp_vote_count = $journalist->votes()->count();
            if ($temp_vote_count > $highest_vote_count) {
                $highest_vote_count = $temp_vote_count;
            }
        }
        return view('journalists.manage',compact('journalists','highest_vote_count'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Journalist  $journalist
     * @return \Illuminate\Http\Response
     */
    public function edit(Journalist $journalist)
    {
        return view('journalists.details',compact('journalist'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Journalist  $journalist
     * @return \Illuminate\Http\Response
     */
    public function update(Journalist $journalist, JournalistRequest $request) //wstrzykuje sie caly obiekt $journalist otrzymujac z route'a tylko id
    {
        if ($request->journalist_image) {
            $image_path=$request->file('journalist_image')->store('images');
            $journalist->image=$image_path;
        }
        $journalist->name=$request->journalist_name;
        $journalist->description=$request->journalist_description;
        $journalist->save();
        return view('layouts.admin')->with('notice',__('messages.your_changes_have_been_saved'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Journalist  $journalist
         * @return \Illuminate\Http\Response
         */
    public function destroy(Journalist $journalist)
    {
        if (Auth::check() && Auth::user()->is_admin) {
        $journalist->delete();   //zabezpieczenie przed usunięciem bez uprawnień
        //po zgadnięciu i wpisaniu linku do usunięcia dziennikarza
        }
        return redirect('admin/manage');
    }

    public function diagramJson()
    {
        $counter=1;
        $journalists=Journalist::all();
        $diagramJson=["title"=>["text"=>__('messages.voting_results')],"data"=>[["dataPoints"=>[]]]];
        foreach ($journalists as $journalist_array_number=>$journalist_object) {
            $diagramJson["data"][0]["dataPoints"][]=["x"=>$counter,"y"=>$journalist_object->votes()->count(),"label"=>$journalist_object->name];
            $counter++;
        }
        return json_encode($diagramJson); //it would return json even without json_encode
    }

    public function showDiagram()
    {
        return view('journalists.diagram');
    }

    public function search(Request $request)
    {
        $queryString = $request->queryString;
        $journalists = Journalist::where('name','like','%'.$queryString.'%')->get();
        $setting=Setting::all()->first();
        $votes_limit_per_ip=$setting->votes_limit_per_ip;
        return view('journalists.index',compact('journalists','votes_limit_per_ip'));
    }
}
