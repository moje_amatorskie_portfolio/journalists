<?php

namespace App\Http\Controllers;

use App\Setting;
use App\Journalist;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $journalist_count=Journalist::all()->count();
        $settings_array=Setting::first();
        $votes_limit_per_ip=$settings_array->votes_limit_per_ip;
        return view('journalists.settings',compact('votes_limit_per_ip','journalist_count'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $journalist_count=Journalist::all()->count();
        $settings=Setting::all()->first();
        $settings->votes_limit_per_ip=$request->set_votes_limit_per_ip;
        $settings->save();
        $votes_limit_per_ip=$request->set_votes_limit_per_ip;
        return view('journalists.settings',compact('votes_limit_per_ip','journalist_count'))->with('notice',__('messages.vote_limit_has_been_set'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function show(Setting $setting)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function edit(Setting $setting)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Setting $setting)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function destroy(Setting $setting)
    {
        //
    }
}
