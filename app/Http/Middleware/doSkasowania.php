<?php

namespace App\Http\Middleware;

use Closure;

class doSkasowania
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        \Illuminate\Support\Facades\View::share('notice','belemobele');
        return $next($request);
    }
}
