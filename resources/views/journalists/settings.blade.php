@extends('layouts.admin')

@section('subtitle')
    &nbsp / &nbsp {{__('messages.settings')}}
@endsection

@if(Auth::check() && Auth::user()->is_admin)
    @section('content_adm_general')
        <div class='pos-center text-center'>
            <form action="{{url('/setting')}}" method='POST'>
                @csrf
                <h6 class=' h-100'>{{__('messages.set_votes_limit_per_ip')}}</h6>
                <input class='w-100 h-100 text-center text-sm' type='number' min='1' max='{{$journalist_count}}' step='1' name='set_votes_limit_per_ip'
                value="{{$votes_limit_per_ip}}"><br>
                <button class='btn btn-warning w-100' type='submit'>{{__('messages.save')}}</button>
            </form>
        </div>
    @endsection
@endif
