@extends('layouts.admin')

@section('subtitle')
&nbsp / &nbsp Dodaj
@endsection

@section('content')
@if(Auth::check() && Auth::user()->is_admin)
 <!-- general form elements -->
<div class="card card-primary pos-center">
  <!-- form start -->
  <form role="form" action="{{url('/add')}}" enctype="multipart/form-data" method='POST'>
    @csrf
    <div class="card-body">
      <div class="form-group">
        <label for="journalist_name">{{__('messages.name')}}</label>
        <input type="text" class="form-control {{ $errors->has('journalist_name') ? ' is-invalid' : '' }}" id="journalist_name" name='journalist_name' placeholder="{{__('messages.enter_name')}}" value="{{old('journalist_name')}}">
        <span class='text-danger'>
          <strong> {{$errors->first('journalist_name')??''}} </strong>
        </span>
      </div>
      <div class="form-group">
        <label for="description">{{__('messages.description')}}</label>
        <textarea id="journalist_description" name="journalist_description" placeholder="{{__('messages.description')}}" class="form-control {{ $errors->has('journalist_description') ? 'is-invalid':''}}" rows=10>{{old('journalist_description')}}</textarea>
        <span class='text-danger'>
          <strong> {{$errors->first('journalist_description')??''}} </strong>
        </span>
      </div>
      <div class="form-group">
        <label for="exampleInputFile">{{__('messages.image')}}</label>
        <div class="input-group">
          <div class="form-group">
            <input type="file" class="{{ $errors->has('journalist_image') ? ' is-invalid' : '' }}" id="image" accept='image/*' name='journalist_image'><br>
            <span class='text-danger'>
              <strong> {{$errors->first('journalist_image')??''}} </strong>
            </span>
          </div>
        </div>
      </div>
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
      <button type="submit" class="btn btn-primary">{{__('messages.save')}}</button>
    </div>
  </form>
</div>
<!-- /.card -->
@endif

@endsection
