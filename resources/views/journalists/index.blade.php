@extends((Auth::check() && Auth::user()->is_admin)?'layouts.admin':'layouts.general')

<?php
$field_hidden = "";
if(Auth::check() && Auth::user()->is_admin)
    $field_hidden = "hidden";
?>

@section('title')
{{__('messages.journalist_ranking')}}<br>
@endsection

@section('content')
    @isset($journalists)
        @foreach ($journalists as $journalist)
            <div class="col-12 col-sm-6 col-md-4 d-flex align-items-stretch journalist-border">
                <div class="card bg-light" >
                    <div class="card-body pt-0">
                        <div class="row">
                            <div class="col-6 text-center float-left">
                                <img src="{{$journalist->image??''}}" alt="image" class="img-circle  img-fluid width-200 height-150">
                            </div>
                            <div class="col-6 float-right">
                                <strong><h3 class="font-size-25 border-bottom">{{$journalist->name??''}}</h3></strong>
                                <h3 class=" text-justify font-size-20 float-right">{!!$journalist->shortDescription??''!!}</h3>
                            </div>
                       </div>
                    </div>
                    <div {{$field_hidden}}>
                        <input id="{{$journalist->id??''}}" name="{{$journalist->id??''}}"  class='float-left' type='checkbox' form='votes_form' onclick="votes_counter( {{$journalist->id??''}} )" {{$field_hidden}}>
                        <label class="cursor-pointer btn btn-info float-right text-xl" for="{{$journalist->id??''}}" {{$field_hidden}}>{{__('messages.vote')}}</label>
                    </div>
                    <button  type='submit' class='btn btn-success' form='votes_form' {{$field_hidden}}>{{__('messages.send_all_votes')}} !</button>
                </div>
            </div>
        @endforeach
    @endisset
    <form id='votes_form' action="{{url('/post_votes')}}" method='POST' hidden>
        @csrf
    </form>
@endsection

@section('script')
    <script>
        var votes_limit_per_ip="{{$votes_limit_per_ip??''}}";  //ta zmienna będzie wyświetlana w alercie
        var count_left=votes_limit_per_ip;
    </script>
    <script src="{{asset('js/votes.js')}}"></script>
@endsection
