@extends((Auth::check() && Auth::user()->is_admin)?'layouts.admin':'layouts.general')

@section('subtitle')
&nbsp / &nbsp Edytuj
@endsection

@section('content')
<?php
$disabled='disabled';
$hidden='hidden';
$rows=11;
$space="<br><br>";
if (Auth::check() && Auth::user()->is_admin) {
    $disabled='';
    $hidden='';
    $rows=7;
    $space="";
}
?>
    <div class=' card card-primary float-left calambur'>
        <img src="../{{$journalist->image}}" width="500px" height="550px">
    </div>
    <!-- general form elements -->
    <div class="card card-primary float-left calambur">
      <form role="form" action="{{url('/update')}}/{{$journalist->id}}" enctype="multipart/form-data" method='POST'>
        @csrf
        <div class="card-body">
          <div class="form-group">
            <label for="journalist_name">{{__('messages.name')}}</label>
            <input type="text" class="form-control {{ $errors->has('journalist_name') ? ' is-invalid' : '' }}" id="journalist_name" name='journalist_name' placeholder="{{__('messages.enter_name')}}" value="{{$journalist->name}}" {{$disabled}}>
            <span class='text-danger'>
              <strong> {{$errors->first('journalist_name')??''}} </strong>
            </span>
          </div>{!!$space!!}
          <div class="form-group">
            <label for="description">{{__('messages.description')}}</label>
            <textarea id="journalist_description" name="journalist_description" placeholder="{{__('messages.description')}}" class="form-control {{ $errors->has('journalist_description') ? 'is-invalid':''}}" rows="{{$rows}}" {{$disabled}}>{{$journalist->description}}</textarea>
            <span class='text-danger'>
              <strong> {{$errors->first('journalist_description')??''}} </strong>
            </span>
        </div>
          <div class="form-group">
            <label for="exampleInputFile" {{$hidden}}>{{__('messages.image')}}</label>
            <div class="input-group">
              <div class="form-group">
                <input type="file" class="{{ $errors->has('journalist_image') ? ' is-invalid' : '' }}" id="image" accept='image/*' name='journalist_image' {{$hidden}}><br>
                <span class='text-danger'>
                  <strong> {{$errors->first('journalist_image')??''}} </strong>
                </span>
              </div>
            </div>
          </div>
        </div>
        <!-- /.card-body -->

        <div class="card-footer" {{$hidden}}>
          <button type="submit" class="btn btn-primary" {{$hidden}}>{{__('messages.save')}}</button>
        </div>
      </form>
    </div>
    <!-- /.card -->

@endsection
