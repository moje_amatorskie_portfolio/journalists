@extends('layouts.admin')

@section('subtitle')
&nbsp / &nbsp Głosy
@endsection

@if(Auth::check() && Auth::user()->is_admin)
    @section('content_adm_general')
        <div class='pos-center'>
            <table class='table table-striped'>
                <th>ip</th>
                <th>{{__('messages.date')}}</th>
                <th>{{__('messages.delete')}}</th>
                @foreach ($votes as $vote)
                    <tr>
                        <td>{{$vote->ip}}</td>
                        <td>{{$vote->updated_at}}</td>
                        <td><a href="{{url('/vote/delete')}}/{{$journalist_id}}/{{$vote->id}}"><button class='btn btn-warning btn-sm'>{{__('messages.delete')}}</button></a></td>
                    </tr>
                @endforeach
            </table>
        </div>
    @endsection
@endif
