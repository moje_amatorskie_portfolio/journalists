@extends('layouts.admin')

@section('subtitle')
&nbsp / &nbsp {{__('messages.manage')}}
@endsection

<?php $i=1;?>

@if(Auth::check() && Auth::user()->is_admin)
@section('content')
    @isset($journalists)
    <div class="card-body p-0" style="display: block;" >
      <table class="table table-striped projects" >
          <thead>
              <tr>
                  <th style="width: 3%">
                      #
                  </th>
                  <th style="width: 22%">
                      {{__('messages.name')}}
                  </th>
                  <th style="width: 15%">
                      {{__('messages.image')}}
                  </th>
                  <th style='width: 20%'>
                      {{__('messages.diagram')}}
                  </th>
                  <th style="width: 10%">
                      {{__('messages.votes')}}
                  </th>
                  <th style="width: 30%">
                      {{__('messages.options')}}
                  </th>
              </tr>
          </thead>
          <tbody  >
            @foreach ($journalists as $journalist)
              <tr>
                  <td>
                      {{$i++}}
                  </td>
                  <td>
                      <a>
                          {{$journalist->name}}
                      </a>
                  </td>
                  <td>
                      <ul class="list-inline">
                          <li class="list-inline-item">
                              <img alt="brak obrazka" class="table-avatar" width=33 height=33 src="../{{$journalist->image}}">
                          </li>
                      </ul>
                  </td>
                  <td class="project_progress">
                      <div class="progress progress-sm">
                          <div class="progress-bar bg-green" role="progressbar" aria-volumenow="57" aria-volumemin="0" aria-volumemax="100" style="width: {{$journalist->votes()->count()*100/($highest_vote_count ? $highest_vote_count : request()->input('highest_vote_count'))}}%"> <!--tutaj wyskakiwał błąd dzielenia przez zero po wprowadzeniu
                              paginacji, więc przez medotę append dołącza się klucz highest_vote_count do każdej strony paginacji -->
                          </div>
                      </div>
                  </td>
                  <td class="project-state">
                      <span class="  float-left">{{$journalist->votes()->count()}}</span>
                  </td>
                  <td class="project-actions text-left">
                      <a class="btn btn-secondary btn-sm" href="{{url('/vote')}}/{{$journalist->id}}">
                          <i class="fas fa-folder">
                          </i>
                          {{__('messages.votes')}}
                      </a>
                      <a class="btn btn-info btn-sm" href="{{url('/edit')}}/{{$journalist->id}}">
                          <i class="fas fa-pencil-alt">
                          </i>
                          {{__('messages.edit')}}
                      </a>
                      <a class="btn btn-danger btn-sm" href="{{url('/delete')}}/{{$journalist->id}}">
                          <i class="fas fa-trash">
                          </i>
                          {{__('messages.delete')}}
                      </a>
                  </td>
              </tr>
            @endforeach
          </tbody>
        </table>
        {{ $journalists->appends(['highest_vote_count'=>$highest_vote_count ? $highest_vote_count : request()->input('highest_vote_count')])->links() }} <!--powyżej wyskakiwał błąd dzielenia przez zero po wprowadzeniu
            paginacji, więc przez medotę append dołącza się klucz highest_vote_count do każdej strony paginacji -->
      </div>
      <!-- /.card-body -->
    @endisset
@endsection
@endif
