@extends((Auth::check() && Auth::user()->is_admin)?'layouts.admin':'layouts.general')

@section('title')
{{__('messages.diagram')}}
@endsection

@section('content')
<div class='pos-center' id='diagram'></div>
<div id="chartContainer" style="height: 300px; width: 100%;">
</div>
@endsection

@section('script_1')
<script>
    $(document).ready(function()
    {
        var xhttp=new XMLHttpRequest();
        xhttp.onreadystatechange=function()
        {
            if (this.readyState==4 && this.status==200) {
                var json_decoded=JSON.parse(this.responseText);
                var chart = new CanvasJS.Chart("chartContainer",json_decoded);
                chart.render();
            }
        };
        xhttp.open('get',"{{url('/diagram_json')}}",true);
        xhttp.send();
    });
</script>
@endsection
