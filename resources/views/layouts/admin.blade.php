<?php

if (isset($journalist_added)) {
    $notice="<br>".__('messages.journalist_successfully_added')."&nbsp<img width=20 src='".asset('images/ok.png')."'>";
}

?>
@extends('layouts.general')

@section('title')
Panel Admin
@yield('subtitle')
@endsection

@section('link_1')
<a href="{{url('/add')}}" class="nav-link">
    <i class="nav-icon fas fa-user-plus"></i>
    <p>
        {{__('messages.add')}}
    </p>
</a>
@endsection

@section('link_2')
<a href="{{url('admin/manage')}}" class="nav-link">
    <i class="nav-icon fas fa-user-edit"></i>
    <p>
        {{__('messages.manage')}}
    </p>
</a>
@endsection

@section('link_4')
<a href="{{url('/setting')}}" class="nav-link">
    <i class="nav-icon fas fa-cog"></i>
    <p>
        {{__('messages.settings')}}
    </p>
</a>
@endsection

@section('errors')
<?php
$decoded_errors=json_decode($errors);
?>
<div class='pos-center'>
    <ul>
        @foreach ($decoded_errors as $field_errors_array)
            @foreach ($field_errors_array as $field_error)
                <h1 class='text-danger overflow-hidden'><li>{{$field_error}}</li></h1>
            @endforeach
        @endforeach
    </ul>
</div>
@endsection

@if((Auth::check()) && (Auth::user()->is_admin))
@yield('content_admin')
@endif

@if(!(Auth::check()) || !(Auth::user()->is_admin))
    @section('content_adm_general')
    <div class='pos-center'>{{__('messages.no_admin_rights')}}</div>
    @endsection
@endif
