<?php
$hidden='';
if (Auth::check() && Auth::user()->email_verified_at){
    $hidden='hidden';
}
?>

<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>@yield('title')</title>
  <link rel="stylesheet" href="{{asset('css/my_blade.css')}}">
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="{{asset('AdminLte/plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('AdminLte/dist/css/adminlte.min.css')}}">
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="{{asset('AdminLte/plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- SweetAlert2 -->
  <link rel="stylesheet" href="{{asset('AdminLte/plugins/sweetalert2/sweetalert2.min.css')}}">
  <!-- Toastr -->
  <link rel="stylesheet" href="{{asset('AdminLte/plugins/toastr/toastr.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('AdminLte/dist/css/adminlte.min.css')}}">
  <!-- Bootstrap -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <script type="text/javascript" src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
</head>
<body class="hold-transition sidebar-mini">
  <div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="{{url('/journalists')}}" class="nav-link">Start</a>
      </li>
    </ul>

    <!-- SEARCH FORM -->
    <form class="form-inline ml-3" method='GET' action={{url('/search')}}>
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" name='queryString' type="search" placeholder="{{__('messages.find_journalist')}}" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fas fa-search"></i>
          </button>
        </div>
      </div>
    </form>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        <ul class="navbar-nav ml-auto">
            <!-- Authentication Links -->
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle text-dark" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{__('language')}} <span class="caret"></span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{url('/lang/en')}}"><img src="{{asset('images/us.png')}}" width="30px" height="20x"> English</a>
                        <a class="dropdown-item" href="{{url('/lang/pl')}}"><img src="{{asset('images/pl.png')}}" width="30px" height="20x"> Polski</a>
                        <a class="dropdown-item" href="{{url('/lang/de')}}"><img src="{{asset('images/de.png')}}" width="30px" height="20x"> Deutsch</a>
                    </div>
                </li>
        </ul>
        @auth
        <li class="nav-item">
          <button type='submit' form='logout_form' class="w-100 btn">
              &nbsp {{__('messages.log_out')}}
          </button>
          <form id='logout_form' action="{{url('/logout')}}" method="POST" hidden>
            @csrf
          </form>
        </li>
        <li class='nav-item'>
          <button type='submit' class='btn'>
            {{Auth::user()->name}}
          </button>
        </li>
        @endauth

        @guest
        <li class="nav-item">
          <a class="nav-link" href="{{ route('register') }}">{{ __('messages.register') }}</a>
        </li>

        <li class="nav-item">
          <a href="{{url('/login')}}" class="nav-link">
            <p class='float-left'>
              &nbsp {{__('messages.log_in')}}
            </p>
          </a>
        </li>
        @endguest


    </ul>




  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">

    <!-- Sidebar -->
    <div class="sidebar">


      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          @if (Auth::check())
          <li class='nav-item'>
              <a href="#" class='nav-link'>
            <h5><strong>{{Auth::user()->name}}</strong></h5>
              </a>
          </li>
          @endif

          @if (Auth::check() && Auth::user()->is_admin)
          <li class='nav-item'>
            @yield('link_1')
          </li>
          <li class="nav-item">
            @yield('link_2')
          </li>
          <li class="nav-item">
            @yield('link_3')
          </li>
          <li class="nav-item">
            @yield('link_5')
          </li>
          <li class="nav-item">
            @yield('link_4')
          </li>
          @endif

          <!-- Lista -->
          <li class='nav-item'>
            <a href="{{url('/journalists')}}" class="nav-link">
              <i class='nav-icon fas fa-grip-lines'></i>
              <p>
                  {{__('messages.list')}}
              </p>
            </a>
          </li>

          <!-- Wykres -->
          <li class="nav-item">
            <a href="{{url('/diagram')}}" class="nav-link">
              <i class="nav-icon fas fa-teeth"></i>
              <p>
                {{__('messages.diagram')}}
              </p>
            </a>
          </li>

          <!-- Wyślij głosy -->
          @auth
          <li class="nav-item">
            <a href="{{url('/email/votes')}}" class="nav-link">
              <i class="nav-icon fas fa-envelope"></i>
              <p>
                {{__('messages.email_votes')}}
              </p>
            </a>
          </li>

          <!-- Zweryfikuj swój email -->
          <li class="nav-item" {{$hidden}}>
            <a href="{{url('/email/verify/link')}}" class="nav-link">
              <i class="nav-icon fas fa-envelope"></i>
              <p class='text-danger'>
                {{__('verify_your_email')}}
              </p>
            </a>
          </li>
          @endauth
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header)  -->
    <div class="content-header">
      <div class="container-fluid">
        <div class='text-left'>@yield('title')</div><br>
        <div class='pos-center'>@yield('errors')</div>
        <div class='pos-center'><!--Ranking Dziennikarzy 7.11.2019--></div>
        <div class='pos-center text-xl text-center'>{!!$notice??''!!}</div>
          <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark text-center"></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">

            </ol>
          </div><!-- /.col -->
            @yield('content')
        </div><!-- /.row -->

            @yield('content_adm_general')

      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <!-- <div class="float-right d-none d-sm-inline">
      Anything you want
    </div> -->
    <!-- Default to the left -->
    <!-- <strong>Copyright &copy; 2014-2019 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved. -->
  </footer>
</div>
<!-- ./wrapper -->



  <!-- REQUIRED SCRIPTS -->

  <!-- jQuery -->
  <script src="{{asset('AdminLte/plugins/jquery/jquery.min.js')}}"></script>
  <!-- Bootstrap 4 -->
  <script src="{{asset('AdminLte/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <!-- AdminLTE App -->
  <script src="{{asset('AdminLte/dist/js/adminlte.min.js')}}"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="{{asset('AdminLte/dist/js/demo.js')}}"></script>

  @yield('script')

  @yield('script_1')

</body>
</html>
