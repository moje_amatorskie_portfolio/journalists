<!DOCTYPE html>
<html lang="pl">
<head>
<style>
  body {  font-family: DejaVu Sans !important; font-size: 12px;}
</style>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
     <link rel="stylesheet" href="{{asset('css/my_blade.css')}}">
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <title>Lista głosów</title>
</head>
<body>
    <h3>Lista głosów</h3>
    <table class="table table-striped">
      <thead class='thead-dark'>
        <tr>
          <th>Nazwa</th>
          <th>Lista głosów</th>
        </tr>
      </thead>
      <tbody>
        @foreach ($journalistVotesMailList as $journalist)
        <tr>
          <td>{{$journalist->name}}</td>
          <td>{{$journalist->votes()->count()}}</td>
        </tr>
        @endforeach
      </tbody>
    </table>
</body>
</html>
