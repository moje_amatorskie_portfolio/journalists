@extends('layouts.general')

@section('content')
<a href="{{url('/login')}}" class="pos-center nav-link active">
    <div class='text-xl'>
        {{__('messages.you_have_been_logged_out')}}
    </div>
</a>
@endsection
