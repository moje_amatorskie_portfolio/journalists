<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Twoje hasło zostało zresetowane!',
    'sent' => 'Wysłaliśmy ci link do zmiany hasła!',
    'token' => 'Ten token hasła jest nieważny',
    'user' => "Nie możemy znaleść użytkownika z tym adresem email",

];
